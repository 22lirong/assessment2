//install modules
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const dbConfig = require('./dbconfig');

//create a connection pool for mysql
const pool = mysql.createPool(dbConfig);

//create an instance of express
const app = express();

//enable CORS for all routes
app.use(cors());

//Configure routes
//select * from BOOKS where title like ? or author_firstname like ? order by title limit ? offset ?
const SQL = 'select * from books where title like ? or author_firstname like ? order by title limit ? offset ?';

app.get('/books', (req, resp) => {
    const title = '%'+req.query['title']+'%';
    const author_firstname = '%'+req.query['author_firstname']+'%';
    const limit = parseInt(req.query.limit) || 10;
    const offset = parseInt(req.query.offset) || 0;
    
    pool.getConnection((err, conn) => {
        if (err) {
            resp.status(500).json({ error: err });
            return;
        }

        conn.query(SQL, [ title, author_firstname, parseInt(limit), parseInt(offset) ],
            (err, results) => {
                try {
                    if (err) {
                        resp.status(400).json({ error: err });
                        return;
                    }
                    resp.status(200);
                    resp.format({
                        'application/json': () => {
                            resp.json(results);
                        },
                        'text/csv': () => {
                            resp.send(toCSV(results));
                        },
                
                        'default': () => {
                            resp.status(406).json({ error: 'Not supported'});
                        }
                    });
                } finally { conn.release(); }
            }
        )
    });
});


//by ID
//select * from books where id = ?
const SQL_BY_ID = 'select * from books where id = ?';

app.get('/book/:bookId', (req, resp) => {
    console.log(`id = ${req.params.bookId}`);

    pool.getConnection((error, conn) => {
        if (error) {
            resp.status(500).json({error: error}); return;
        }
        conn.query(SQL_BY_ID, [ req.params.bookId ],
            (error, result) => {
                try {
                    if (error) {
                        resp.status(400).json({error: error});
                        return;
                    }
                    if (result.length)
                        resp.status(200).json(result[0]);
                    else
                        resp.status(404).json({error: 'Not Found'});
                } finally { conn.release(); }
            }
        )
    })
});



app.use(express.static(__dirname + '/public'));


const toCSV = function(results) {

    if (!(results && results.length)) 
        return ('');

    let csv = ''

    //Create the header
    let c = []
    let headers = Object.keys(results[0]);
    csv = headers.join(',') + '\r\n';

    for (let rec of results) { 
        c = []
        for (let field of headers)
            c.push(rec[field])
        csv = csv + c.join(',') + '\r\n';
    }

    return (csv);
}

//set port
const PORT = process.argv[2] || process.env.APP_PORT || 3000;

//Only start the application if the db is up
console.log('Pinging database...');
pool.getConnection((err, conn) => {
    if (err) {
        console.error('>Error: ', err);
        process.exit(-1);
    }

    conn.ping((err) => {
        if (err) {
            console.error('>Cannot ping: ', err);
            process.exit(-1);
        }
        conn.release();

        //Start application only if we can ping database
        app.listen(PORT, () => {
            console.log('Starting application on port %d', PORT);
        });
    });
});
