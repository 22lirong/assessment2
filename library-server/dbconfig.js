const CONFIG = {
    host: 'localhost', port: 3306,
    user: 'rong', password: 'rongrong',
    database: 'library',
    connectionLimit: 4
};

module.exports = CONFIG;