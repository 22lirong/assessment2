import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import 'rxjs/add/operator/take';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BookService {

  constructor(private httpClient: HttpClient) { }

  getAllBooks(config = { }): Promise<any> {

      let qs = new HttpParams()

      .set('title', config['title'])
      .set('author_firstname', config['author_firstname'])
      .set('limit', config['limit'] || 10 )
      .set('offset', config['offset'] || 0 );
  
    return (
      this.httpClient.get('http://localhost:3000/books', { params: qs })
        .take(1).toPromise()
    );
  }

  //GET /book/<bookId>
  getBook(bookId: number): Promise<any> {
    return (
      this.httpClient.get(`http://localhost:3000/book/${bookId}`)
        .take(1).toPromise()
    );
  }


}
