import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BookService } from '../book.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  @ViewChild(NgForm) bookForm: NgForm;

  books = [];

  constructor(private bookSvc: BookService) { }

  ngOnInit() { }

  getBooks(_bookForm: NgForm) {
    
    const title = this.bookForm.value['title'];
    const author_firstname = this.bookForm.value['author_firstname'];
    const limit = this.bookForm.value['limit'] || 10;
    const offset = this.bookForm.value['offset'] || 0;
    
    console.log('> title: ', this.bookForm.value.title);
    console.log('> author_firstname: ', this.bookForm.value.author_firstname);
    console.log('> limit: ', limit);
    console.log('> offset: ', offset);
    
    this.bookSvc.getAllBooks({ title, author_firstname, limit, offset })
      .then(result => {
        console.log('>>> result: ', result);
        this.books = result
      })
      .catch(error => {
        console.log('error: ', error);
      })

  }
}

